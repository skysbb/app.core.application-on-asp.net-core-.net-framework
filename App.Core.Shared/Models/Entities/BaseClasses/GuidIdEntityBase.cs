﻿using System;

namespace App.Core.Shared.Models.Entities.BaseClasses
{
    public abstract class GuidIdEntityBase : IHasGuidId {
        public Guid Id { get; set; }

        public GuidIdEntityBase()
        {
            Id = GuidFactory.NewGuid();
        }
    }
}
