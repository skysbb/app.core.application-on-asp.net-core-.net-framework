namespace App.Core.Infrastructure.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntitiesExample : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Example",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        BooleanValue = c.Boolean(nullable: false),
                        NumberValue = c.Int(nullable: false),
                        TextValue = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Example");
        }
    }
}
