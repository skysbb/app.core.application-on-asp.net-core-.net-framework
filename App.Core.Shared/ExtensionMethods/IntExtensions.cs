﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Note that Namespace is truncated to root 
// so that methods are available throughout app

namespace App.Core
{
    public static class IntegerExtensions
    {
        /// <summary>
        /// Converts an integer to a Guid unique identifier
        /// (obviously not very random).
        /// <para>
        /// Useful for initial seeding scenarios.
        /// </para>
        /// </summary>
        /// <returns></returns>
        public static Guid ToGuid(this int value)
        {
            byte[] bytes = new byte[16];
            BitConverter.GetBytes(value).CopyTo(bytes, 0);
            return new Guid(bytes);
        }

    }
}
