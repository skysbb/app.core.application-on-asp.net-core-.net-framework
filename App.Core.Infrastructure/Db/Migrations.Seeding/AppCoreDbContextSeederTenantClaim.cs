﻿using App.Core.Infrastructure.Db.Context;
using App.Core.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Db.Migrations.Seeding
{
    public static class AppCoreDbContextSeederTenantClaim
    {
        public static void Seed(AppCoreDbContext context)
        {
            var records = new TenantClaim[] {
                new TenantClaim {Id=1.ToGuid(), OwnerFK=1.ToGuid(), Authority=null, AuthoritySignature=null, Key="SomePropA",Value="SomeValueA1"},
                new TenantClaim {Id=2.ToGuid(), OwnerFK=1.ToGuid(), Authority=null, AuthoritySignature=null, Key="SomePropB",Value="SomeValueB1"},

                new TenantClaim {Id=3.ToGuid(), OwnerFK=2.ToGuid(), Authority=null, AuthoritySignature=null, Key="SomePropA",Value="SomeValueA1"},
                new TenantClaim {Id=4.ToGuid(), OwnerFK=2.ToGuid(), Authority=null, AuthoritySignature=null, Key="SomePropB",Value="SomeValueB1"},

            };
            context.Set<TenantClaim>().AddOrUpdate<TenantClaim>(p => p.Id, records);
        }
    }
}

