﻿using System;

namespace App.Core.Shared.Models.Messages.V0100
{
    public class ExampleDto
    {
        public Guid Id { get; set; }
        public bool BooleanValue { get; set; }
        public int NumberValue { get; set; }
        public string TextValue { get; set; }
    }
}
