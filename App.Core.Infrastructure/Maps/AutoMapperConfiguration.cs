﻿using App.Core.Infrastructure.Maps.DTOs.V0100;
using AutoMapper;

namespace FooDemoServer.Presentation.Mappers
{


    public static class AutoMapperConfiguration
    {
        public static void Initialize()
        {
            Mapper.Initialize(cfg =>
            {
                Map_Tenant_TenantDto.Initialize(cfg);
                Map_TenantProperty_TenantPropertyDto.Initialize(cfg);
                Map_TenantClaim_TenantClaimDto.Initialize(cfg);
                //Map_Principal_UserDto.Initialize(cfg);
                //Map_PrincipalProperty_UserPropertyDto.Initialize(cfg);
                //Map_PrincipalClaim_UserClaimDto.Initialize(cfg);
                //Map_Session_SessionDto.Initialize(cfg);
                //Map_SessionOperation_SessionOperationDto.Initialize(cfg);

                //Map_FooItem_FooItemDto.Initialize(cfg);

                //Map_FooSubItem_FooSubItemDto.Initialize(cfg);

                //Map_FooItem_SearchResponseItemDto.Initialize(cfg);

                //Map_Message_MessageDto.Initialize(cfg);

                //Map_Notification_NotificationDto.Initialize(cfg);

                //Map_BodyChannel_BodyChannelDto.Initialize(cfg);

                //Map_BodyProperty_BodyPropertyDto.Initialize(cfg);

                //Map_BodyClaim_BodyClaimDto.Initialize(cfg);

                //Map_BodyCategory_BodyCategoryDto.Initialize(cfg);

                //Map_BodyAlias_BodyAliasDto.Initialize(cfg);

                //Map_Body_BodyDto.Initialize(cfg);

            });

            Mapper.AssertConfigurationIsValid();
        }
    }
}
