﻿using App.Core.Infrastructure.Db.Context;
using App.Core.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Db.Migrations.Seeding
{
    public static class AppCoreDbContextSeederTenant

    {
        public static void Seed(AppCoreDbContext context)
        {

            var records = new Tenant[] {
                //People:
                new Tenant {Id=1.ToGuid(), Enabled=true, Key="OrgA", DisplayName="Org A, Inc."},
                new Tenant {Id=2.ToGuid(),  Enabled=true, Key="OrgB", DisplayName="Org B, Inc."},

            };
            context.Set<Tenant>().AddOrUpdate<Tenant>(p => p.Id, records);
            context.SaveChanges();
        }
    }


}
