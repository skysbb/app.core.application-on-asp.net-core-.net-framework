﻿namespace App.Core.Shared.Models
{
    public interface IHasIntId : IHasId<int>
    {

    }
}
