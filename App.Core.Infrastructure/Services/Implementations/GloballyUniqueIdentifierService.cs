﻿using System;

namespace App.Core.Infrastructure.Services.Implementations
{
    public class GloballyUniqueIdentifierService : IGloballyUniqueIdentifierService
    {
        /// <summary>
        /// Generate a new sequentially random Guid.
        /// </summary>
        /// <returns></returns>
        public Guid NewGuid()
        {
            return App.Core.GuidFactory.NewGuid();
        }
    }
}
