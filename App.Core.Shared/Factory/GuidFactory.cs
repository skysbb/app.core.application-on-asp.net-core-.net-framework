﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Note that in order to make this class available in as many places as possible
// the namespace was modified/truncated.
namespace App.Core
{
    // This is the only Factory developed in the Shared space, rather than Infrastructure.
    // As Models need it in the constructor. 

        
    public static class GuidFactory
    {
        
        public static Guid NewGuid()
        {
            //TODO: replace with the generation of a sequentially random Guid.
            return Guid.NewGuid();
        }
    }
}
