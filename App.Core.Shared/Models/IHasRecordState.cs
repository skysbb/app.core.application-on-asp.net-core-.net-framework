﻿using App.Core.Shared.Models.Entities;


namespace App.Core.Shared.Models
{
    public interface IHasRecordState
    {
        RecordState RecordState { get; set; }
    }
}
