﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Shared.Models.Entities
{
    public class Example : GuidIdEntityBase
    {
        public bool BooleanValue { get; set; }
        public int NumberValue { get; set; }
        public string TextValue { get;set;}
    }
}
