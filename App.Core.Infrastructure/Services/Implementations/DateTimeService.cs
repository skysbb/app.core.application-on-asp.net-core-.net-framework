﻿using System;

namespace App.Core.Infrastructure.Services.Implementations
{
    public class DateTimeService :IDateTimeService
    {
        public DateTimeOffset NowUtc { get { return DateTimeOffset.Now; }}
    }
}
