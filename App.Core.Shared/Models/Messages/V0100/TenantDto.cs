﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace App.Core.Shared.Models.Messages.V0100
{
    public class TenantDto : IHasGuidId, IHasEnabled
    {
        public Guid Id { get; set; }

        public bool Enabled { get; set; }
        public string Key { get; set; }
        public string DisplayName { get; set; }

        public ICollection<TenantPropertyDto> Properties { get { if (_properties == null) { _properties = new Collection<TenantPropertyDto>(); } return _properties; } set { _properties = value; } }
        ICollection<TenantPropertyDto> _properties;

        public ICollection<TenantClaimDto> Claims { get { if (_claims == null) { _claims = new Collection<TenantClaimDto>(); } return _claims; } set { _claims = value; } }
        ICollection<TenantClaimDto> _claims;

    }

}
