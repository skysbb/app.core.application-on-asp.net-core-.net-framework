﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData;
using System.Data.Entity;
using App.Core.Infrastructure.Db.Context;

namespace App.Core.Application.API.Controllers
{
    public abstract class ODataControllerBase<T> : ODataControllerBase where T : class
    {
        protected DbSet<T> _dbSet {get {return _dbContext.Set<T>();}}
        public ODataControllerBase(DbContext dbContext):base(dbContext) {}
    }

    public abstract class ODataControllerBase : ODataController
    {
        protected DbContext _dbContext;

        public ODataControllerBase(DbContext dbContext)
        {
            _dbContext = dbContext;
            //_dbContext.Database.Log = Console.Write;
        }
        protected override void Dispose(bool disposing)
        {
            _dbContext.SaveChanges();
            //If not using DI: if (_dbContext != null) {_dbContext.Dispose();}
            base.Dispose(disposing);
        }
    }
}
