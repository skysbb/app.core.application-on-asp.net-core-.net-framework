namespace App.Core.Infrastructure.Db.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EntitiesTenant : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tenant",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        Enabled = c.Boolean(nullable: false),
                        Key = c.String(),
                        DisplayName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TenantClaim",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Authority = c.String(),
                        AuthoritySignature = c.String(),
                        Key = c.String(),
                        Value = c.String(),
                        Tenant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenant", t => t.Tenant_Id)
                .Index(t => t.Tenant_Id);
            
            CreateTable(
                "dbo.TenantProperty",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        OwnerFK = c.Guid(nullable: false),
                        RecordState = c.Int(nullable: false),
                        Key = c.String(),
                        Value = c.String(),
                        Tenant_Id = c.Guid(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Tenant", t => t.Tenant_Id)
                .Index(t => t.Tenant_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TenantProperty", "Tenant_Id", "dbo.Tenant");
            DropForeignKey("dbo.TenantClaim", "Tenant_Id", "dbo.Tenant");
            DropIndex("dbo.TenantProperty", new[] { "Tenant_Id" });
            DropIndex("dbo.TenantClaim", new[] { "Tenant_Id" });
            DropTable("dbo.TenantProperty");
            DropTable("dbo.TenantClaim");
            DropTable("dbo.Tenant");
        }
    }
}
