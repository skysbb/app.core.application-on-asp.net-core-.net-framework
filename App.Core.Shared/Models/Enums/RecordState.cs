﻿namespace App.Core.Shared.Models
{
    public enum RecordState
    {
        Active=0,
        //Pretty sure this is invalid for a Record State -- ok for an EntityState, but that's different: Disabled=1,
        Archived = 2,
        Merged = 3,
        Garbage = 4,
        Deleted = 5
    }
}
