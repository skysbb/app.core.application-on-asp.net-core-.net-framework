﻿using App.Core.Shared.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace App.Core.Shared.Models.Entities
{
    public class Tenant : GuidIdEntityBase, IHasKey, IHasEnabled
    {
        public bool Enabled { get; set; }

        public string Key { get; set; }

        public string DisplayName { get; set; }

        public ICollection<TenantProperty> Properties { get { if (_properties == null) { _properties = new Collection<TenantProperty>(); } return _properties; } set { _properties = value; } }
        ICollection<TenantProperty> _properties;

        public ICollection<TenantClaim> Claims { get { if (_claims == null) { _claims = new Collection<TenantClaim>(); } return _claims; } set { _claims = value; } }
        ICollection<TenantClaim> _claims;


    }

}
