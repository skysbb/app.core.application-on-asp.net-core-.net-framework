﻿using App.Core.Infrastructure.Db.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Db.Migrations.Seeding
{
    public class SeedingOrchestrator
    {
        public void Initialize(AppCoreDbContext context)
        {
            AppCoreDbContextSeederTenant.Seed(context);
            AppCoreDbContextSeederTenantProperty.Seed(context);
            AppCoreDbContextSeederTenantClaim.Seed(context);
        }

    }
}
