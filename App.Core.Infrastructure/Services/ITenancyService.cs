﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Services
{
    /// <summary>
    /// The Contract for a Service to manage the current Session Principal's Tenancy.
    /// </summary>
    public interface ITenancyService
    {
    }
}
