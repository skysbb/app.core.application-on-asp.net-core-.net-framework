﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Shared.Models
{
    public interface IHasTenantFK
    {
        Guid TenantFK { get; set; }
    }
}
