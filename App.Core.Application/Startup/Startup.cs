﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
//Breaking change using Microsoft.AspNetCore.OData.Extensions; //Hence why app.UseOData<...>() is not available.
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using StructureMap;
using App.Core.Infrastructure.Services;
using App.Core.Domain.Services;
using App.Core.Shared.Models;
using App.Core.Infrastructure.Db.Context;
using StructureMap.Pipeline;
using FooDemoServer.Presentation.Mappers;

namespace App.Core.Application.Startup
{
    public partial class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            // SETUP STEP: 
            AutoMapperConfiguration.Initialize();

            // SETUP STEP: Configure StructureMap:
            services.AddStructureMap();
            
            services
                .AddCors()
                // Add framework services.
                .AddMvc()
                // SETUP STEP: Add our app services:
                .AddControllersAsServices();

            
            // SETUP STEP: Configure the IoC container
            return ConfigureIoC(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            ConfigureLogging(loggerFactory);

            //app.useStructureMap();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            // SETUP STEP: Add Cors:
            app.UseCors(x=>x.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin());

            // SETUP STEP: Add OData:
            //app.UseOData("api");

            ConfigureAccessToStaticResources(app);

            ConfigureMVC(app);

            ConfigureOData(app);
        }

        private void ConfigureLogging(ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(this.Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
        }

        private static void ConfigureAccessToStaticResources(IApplicationBuilder app)
        {
            app.UseStaticFiles();
        }

        private static void ConfigureMVC(IApplicationBuilder app)
        {
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        private static void ConfigureOData(IApplicationBuilder app)
        {
            //DevelopODataSchema(config);

        }

        private IServiceProvider ConfigureIoC(IServiceCollection services)
        {
            //services.AddTransient<ITestService, TestService>();
            return ConfigureIoCUsingStructureMap(services);
        }

        private IServiceProvider ConfigureIoCUsingStructureMap(IServiceCollection services)
        {
            //To ensure we get only one of these per request:
            //services.AddScoped<IUnitOfWork, UnitOfWork>( provider => new UnitOfWork(priority: 3));
            var container = new Container();
            container.Configure(config =>
            {
                //Scan for Application Services through Application, Infrastructure, Domain, and Model assemblies
                config.Scan(x =>
                {
                    x.AssemblyContainingType<Startup>();
                    x.AssemblyContainingType<IDateTimeService>();
                    x.AssemblyContainingType<IExampleDomainService>();
                    x.AssemblyContainingType<IHasGuidId>();
                    x.WithDefaultConventions();
                    //x.Convention<IRegistrationConvention>();
                    //x.AddAllTypesOf<IFooService>();
                    //x.ConnectImplementationsToTypesClosing(typeof(IValidator<>));
                });

                config.For<AppCoreDbContext>()
                .LifecycleIs(new UniquePerRequestLifecycle())
                .Use(x => new AppCoreDbContext(this.Configuration.GetConnectionString("AppCoreDbContext")))
                ;

                //config.For<IConfigurationRoot>()
                //.Singleton().Use(x => this.Configuration)
;

                //@"Data Source=(localdb)\mssqllocaldb;Database=AppCoreDb;Integrated Security=True"

                //Not needed anymore (due to above):config.For(typeof(ITestService)).Use(typeof(TestService));
                config.Populate(services);
            });
            var check =  container.WhatDoIHave();
            return container.GetInstance<IServiceProvider>();
        }
    }
}
