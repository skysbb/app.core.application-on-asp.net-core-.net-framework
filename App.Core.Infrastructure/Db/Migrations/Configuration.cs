namespace App.Core.Infrastructure.Db.Migrations
{
    using App.Core.Infrastructure.Db.Context;
    using App.Core.Infrastructure.Db.Migrations.Seeding;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<App.Core.Infrastructure.Db.Context.AppCoreDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Db\Migrations";
        }

        protected override void Seed(AppCoreDbContext context)
        {
            // For SOC reasons - and because there will soon enough be lots of models -
            // move seeding out to its own class:
            new SeedingOrchestrator().Initialize(context);
        }
    }
}
