﻿using System;


namespace App.Core.Shared.Models
{
    public interface IHasOwnerFK {
        Guid OwnerFK { get; set; }
    }

}
