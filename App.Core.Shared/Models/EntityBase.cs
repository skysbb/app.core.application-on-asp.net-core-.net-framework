﻿using App.Core.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Shared.Models
{
    public abstract class GuidIdEntityBase : IHasGuidId
    {
        public Guid Id { get; set; }

        public GuidIdEntityBase()
        {
            // TODO: for Db performance reasons, 
            // needs a *sequential* random Id generator
            // rather than the default *random* Id generator.
            this.Id = Guid.NewGuid();
        }

        
    }
}
