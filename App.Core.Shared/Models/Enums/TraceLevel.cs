﻿namespace App.Core.Shared.Models
{
    public enum TraceLevel {
        Critical,
        Error,
        Warn,
        Info,
        Debug,
        Verbose
    }
}
