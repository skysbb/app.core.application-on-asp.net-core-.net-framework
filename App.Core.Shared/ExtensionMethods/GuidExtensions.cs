﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// Note that Namespace is truncated to root 
// so that methods are available throughout app

namespace App.Core
{
    public static class GuidExtensions
    {
        public static Guid Generate(this Guid guid)
        {
            return GuidFactory.NewGuid();
        }
    }
}
