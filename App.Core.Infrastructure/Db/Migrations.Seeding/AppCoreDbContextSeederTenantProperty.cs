﻿using App.Core.Infrastructure.Db.Context;
using App.Core.Shared.Models.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Db.Migrations.Seeding
{
    public static class AppCoreDbContextSeederTenantProperty
    {
        public static void Seed(AppCoreDbContext context)
        {
            var records = new TenantProperty[] {
                new TenantProperty {Id=1.ToGuid(), OwnerFK=1.ToGuid(), Key="SomePropA",Value="SomeValueA1"},
                new TenantProperty {Id=2.ToGuid(), OwnerFK=1.ToGuid(), Key="SomePropB",Value="SomeValueB1"},
                };
            context.Set<TenantProperty>().AddOrUpdate<TenantProperty>(p => p.Id, records);
            context.SaveChanges();
        }
    }


}

