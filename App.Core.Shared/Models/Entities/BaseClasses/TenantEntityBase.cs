﻿using System;

namespace App.Core.Shared.Models.Entities.BaseClasses
{

    public abstract class TenantEntityBase : GuidIdEntityBase, IHasTenantFK
    {

        public Tenant Tenant { get; set; }
        public Guid TenantFK { get; set; }

    }
}
