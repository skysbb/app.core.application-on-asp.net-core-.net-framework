﻿using App.Core.Infrastructure.Db.Schema;
using App.Core.Shared.Models.Entities;
using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace App.Core.Infrastructure.Db.Context
{
    public class AppCoreDbContext : DbContext
    {
        public DbSet<Tenant> Tenants { get; set; }
        public DbSet<Example> Examples { get; set; }
        //public AppCoreDbContext() : base("AppCoreDbContext") {}

        public AppCoreDbContext(string connectionString) : base(connectionString) { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            //To keep DbContext focused (SOC...) move Schema defining to a external class:
            new ModelBuilderOrchestrator().Initialize(modelBuilder);
        }
    }
}
