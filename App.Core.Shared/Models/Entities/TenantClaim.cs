﻿using System;

namespace App.Core.Shared.Models.Entities
{
    public class TenantClaim : GuidIdEntityBase, IHasRecordState, IHasOwnerFK
    {
        public Guid OwnerFK { get; set; }
        public virtual RecordState RecordState { get; set; }
        public string Authority { get; set; }
        public virtual string AuthoritySignature { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }




}
