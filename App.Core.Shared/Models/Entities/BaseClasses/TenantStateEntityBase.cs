﻿namespace App.Core.Shared.Models.Entities.BaseClasses
{
    public abstract class TenantStateEntityBase : TenantEntityBase, IHasRecordState
    {
        public RecordState RecordState { get; set; }

    }
}
