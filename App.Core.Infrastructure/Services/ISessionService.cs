﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Services
{
    /// <summary>
    /// Contract for a Service to manage the current Session.
    /// From the Session, the current Principal (Anonymous or Authenticated) 
    /// can be obtained. 
    /// </summary>
    public interface ISessionService
    {
        
    }
}
