﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Web.OData.Routing;
using App.Core.Shared.Models.Entities;
using App.Core.Shared.Models.Messages.V0100;
using App.Core.Infrastructure.Db.Context;

namespace App.Core.Application.API.Controllers.V0100
{
    [Route("api/[controller]")]
    //[ODataRoutePrefix("tenant")]
    public class TenantController : ODataControllerBase<Tenant>
    {


        public TenantController(AppCoreDbContext dbContext):base(dbContext)
        {

        }

        // GET api/values 
        //[ApplyDataContractResolver]
        //[ApplyProxyDataContractResolverAttribute]
        //[ODataRoute()]
        [EnableQuery(PageSize=100)]
        public IQueryable<TenantDto> Get()
        {
            IQueryable <TenantDto> results;
            results = 
                _dbSet
               //Can prefilter as needed: .Where(x => x.Enabled == true )
                .ProjectTo<TenantDto>(x=>x.Properties,x=>x.Claims);
            return results;

        }

        //[ODataRoute("({key})")]
        public TenantDto Get(Guid key)
        {
             return _dbSet.ProjectTo<TenantDto>().SingleOrDefault(x=>x.Id==key);
        }
    }
}