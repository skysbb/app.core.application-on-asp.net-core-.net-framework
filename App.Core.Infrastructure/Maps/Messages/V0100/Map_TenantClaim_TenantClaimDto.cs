﻿using App.Core.Shared.Models.Messages.V0100;
using App.Core.Shared.Models.Entities;
using AutoMapper;

namespace App.Core.Infrastructure.Maps.DTOs.V0100
{
    public class Map_TenantClaim_TenantClaimDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<TenantClaim, TenantClaimDto>()
                      .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.TenantFK, opt => opt.MapFrom(s => s.OwnerFK))
                      .ForMember(t => t.RecordState, opt => opt.MapFrom(s => s.RecordState))
                      .ForMember(t => t.AuthorityKey, opt => opt.MapFrom(s => s.Authority))
                      .ForMember(t => t.Key, opt => opt.MapFrom(s => s.Key))
                      .ForMember(t => t.Value, opt => opt.MapFrom(s => s.Value))
                      .ForMember(t => t.Signature, opt => opt.MapFrom(s => s.AuthoritySignature))
                      ;

        }
    }



}
