﻿using App.Core.Shared.Models.Entities;
using System;

namespace App.Core.Shared.Models.Messages.V0100
{
    public class TenantPropertyDto : IHasGuidId, IHasTenantFK, IHasRecordState
    {
        public Guid Id { get; set; }
        public Guid TenantFK { get; set; }
        public virtual RecordState RecordState { get; set; }

        public string Key { get; set; }
        public string Value { get; set; }
    }
}
