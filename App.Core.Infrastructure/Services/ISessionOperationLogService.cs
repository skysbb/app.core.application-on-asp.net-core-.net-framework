﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Services
{
    /// <summary>
    /// Contract for a Service to record SessionOperationLog entries against the current
    /// Session, which is associated to the current Principal.
    /// </summary>
    public interface ISessionOperationLogService
    {
    }
}
