﻿using App.Core.Shared.Models.Messages.V0100;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData.Builder;
using System.Web.OData.Extensions;

namespace App.Core.Application.Startup
{
    public partial class Startup
    {

        // Entities need to be 'described'. This was a design decision in order for
        // OData to manage other sources than just EF. 
        // Since I'm pushing out DTOs this actually works for us.
        // The most important req is to define what is the Key of an entity.
        //
        // At first, this seems to suck. 
        // You think that if you had used DataAnnotations on your EF model
        // instead of FluentAPI definitions, you could have benefited from 
        // reuse here.
        // But that probably would be an incorrect conclusion: a maintainable 
        // app uses DTOs for APIs and never exposes the real db entities over APIs. 
        // So you would *still* have to define the API's relationships here...
        public static void DevelopODataSchema(HttpConfiguration config)
        {
            //Now define the Odata mapping (why it's not the same as above, dunno):
            //Add this to get odata to work.
            //Can be: EdmModel|ODataModelBuilder|ODataConventionModelBuilder:
            ODataModelBuilder odataModelBuilder = new ODataConventionModelBuilder();

            DefineODataEntitySets(odataModelBuilder);

            // Key tasks when defining OData relationships is ensuring 
            // it knows what is the ID for every entity. Since I use 'Id' instead of the 
            // general convention of 'FooId', I have to define them by hand... bummer.
            DefineODataEntityIndexProperties(odataModelBuilder);

            // The next task is to define the relationship between objects.
            DefineODataEntityRelationships(odataModelBuilder);

            ////Make a route
            //config.MapODataServiceRoute(
            //    routeName: "ODataRoute",
            //    routePrefix: "api",
            //    model: odataModelBuilder.GetEdmModel());

        }



        static void DefineODataEntitySets(ODataModelBuilder odataModelBuilder)
        {

            //How this works is you define an Entity, and map it to a string, that gets converted into a path.
            //So...say you enter http://localhost:9000/api/bodydtotest/
            //and because you said above that any url that starts with 'api' gets handled 
            //via odata, it then tries to find a controller called BodyDtoTestController. 
            //But even if you have a controller called BodyDtoTestController, it won't route to it,
            //until you've made a EntitySet named as 'bodydtotest':
            odataModelBuilder.EntitySet<TenantDto>("tenantdto");
            odataModelBuilder.EntitySet<ExampleDto>("exampledto");
        }


        static void DefineODataEntityIndexProperties(ODataModelBuilder odataModelBuilder)
        {
            odataModelBuilder.EntityType<TenantDto>().HasKey(x => x.Id);
            odataModelBuilder.EntityType<ExampleDto>().HasKey(x => x.Id);

        }
        static void DefineODataEntityRelationships(ODataModelBuilder odataModelBuilder)
        {
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Category);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Names);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Properties);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Claims);
            //odataModelBuilder.EntityType<BodyDto>().HasOptional(x => x.Channels);


        }

    }
}
