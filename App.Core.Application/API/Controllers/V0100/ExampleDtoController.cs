﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Data.Entity;
using System.Web.OData;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Web.OData.Routing;
using App.Core.Shared.Models.Entities;
using App.Core.Shared.Models.Messages.V0100;
using App.Core.Infrastructure.Db.Context;
using App.Core.Shared.Models.Entities;
using App.Core.Application.API.Controllers.V0100.Contracts;

namespace App.Core.Application.API.Controllers.V0100
{
    [Route("api/[controller]")]
    public class ExampleController : ODataControllerBase<Example>, IExampleController
    {
        public ExampleController(AppCoreDbContext dbContext):base(dbContext){}

        [EnableQuery(PageSize=100)]
        public /*IQueryable<ExampleDto>*/ IEnumerable<ExampleDto> Get()
        {
            IQueryable <ExampleDto> results;
            results = _dbSet
               //Can prefilter as needed: .Where(x => x.Enabled == true )
                .ProjectTo<ExampleDto>(/*Include child objects as needed x=>x.Properties,x=>x.Claims */);
            return results;
        }

        public ExampleDto Get(Guid key)
        {
             return _dbSet.ProjectTo<ExampleDto>().SingleOrDefault(x=>x.Id==key);
        }
    }
}