﻿using System;


namespace App.Core.Shared.Models
{
    public interface IHasGuidId :IHasId<Guid>
    {
    }

}
