﻿using System;

namespace App.Core.Shared.Models
{
    public interface IHasDateTimeCreatedUtc
    {
        DateTime DateTimeCreatedUtc {get;set;}
    }
}
