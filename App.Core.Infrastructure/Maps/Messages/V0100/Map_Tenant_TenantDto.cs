﻿using App.Core.Shared.Models.Messages.V0100;
using App.Core.Shared.Models.Entities;
using AutoMapper;

namespace App.Core.Infrastructure.Maps.DTOs.V0100
{
    public class Map_Tenant_TenantDto
    {
        public static void Initialize(IMapperConfigurationExpression config)
        {
            config.CreateMap<Tenant, TenantDto>()
                      .ForMember(t => t.Id, opt => opt.MapFrom(s => s.Id))
                      .ForMember(t => t.Enabled, opt => opt.MapFrom(s => s.Enabled))
                      .ForMember(t => t.Key, opt => opt.MapFrom(s => s.Key))
                      .ForMember(t => t.DisplayName, opt => opt.MapFrom(s => s.DisplayName))

                      .ForMember(t => t.Properties, opt => opt.MapFrom(s => s.Properties))
                      .ForMember(t => t.Claims, opt => opt.MapFrom(s => s.Claims))
                      ;

        }
    }



}
