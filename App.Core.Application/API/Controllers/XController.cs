using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using App.Core.Shared.Models.Entities;
using System.Web.OData;
using App.Core.Infrastructure.Db.Context;
using App.Core.Shared.Models.Messages.V0100;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using System.Web.OData.Query;

namespace App.Core.Application.API.Controllers
{
    [Produces("application/json")]
    [Route("api/X")]
    public class XController : Controller
    {
        AppCoreDbContext _context;
        public XController(AppCoreDbContext context)
        {
            _context = context;
        }
        //[Queryable]
        [EnableQuery(PageSize = 100,MaxExpansionDepth =3, AllowedQueryOptions = System.Web.OData.Query.AllowedQueryOptions.All)]
        public IQueryable<Tenant> Get()
        {
            //ODataQueryOptions queryOptions;
            //queryOptions.ApplyTo 
            return _context.Tenants;//.ProjectTo<TenantDto>(x=>x.Properties,x=>x.Claims);
        }
    }
}