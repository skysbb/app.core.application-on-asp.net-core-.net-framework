﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Infrastructure.Db.Schema
{
    public class ModelBuilderOrchestrator
    {
        public void Initialize(DbModelBuilder modelBuilder) {
            //Invoke *n* methods to initialize the Schema of the DbContext.
            InitializeTodo(modelBuilder);
            //etc...
        }
        public void InitializeTodo(DbModelBuilder modelBuilder)
        {
            //Define the schema of a single Entity here.
        }
        //etc...
    }
}
