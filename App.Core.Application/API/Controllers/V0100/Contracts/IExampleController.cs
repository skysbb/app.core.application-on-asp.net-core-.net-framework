﻿using App.Core.Shared.Models.Messages.V0100;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace App.Core.Application.API.Controllers.V0100.Contracts
{
    public interface IExampleController
    {
        IEnumerable<ExampleDto> Get();
    }
}
