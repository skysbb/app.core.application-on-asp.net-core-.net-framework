﻿using System;

namespace App.Core.Shared.Models
{
    public interface IHasUserFK
    {
        Guid UserFK { get; set; }
    }
}
