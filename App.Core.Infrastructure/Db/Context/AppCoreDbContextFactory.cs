﻿using System.Data.Entity.Infrastructure;

namespace App.Core.Infrastructure.Db.Context
{
    public class AppCoreDbContextFactory : IDbContextFactory<AppCoreDbContext>
    {
        public AppCoreDbContext Create()
        {
            return new AppCoreDbContext(@"Data Source=(localdb)\mssqllocaldb;Database=AppCoreDb;Integrated Security=True");
        }
    }
}
